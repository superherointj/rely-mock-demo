# FROM superherointj/archlinux-esy
# Bellow image is just cache for esy, if you want to build WITHOUT cache use image above. Esy is too slow building first time. 
FROM superherointj/batci:libc-arch-bootstrap

COPY Makefile *.json *.opam *.ml *.mli *.re *.rei dune dune-project ./

COPY test/sampleData/* ./test/sampleData/
 
RUN esy 

RUN esy build

RUN esy x RunTests

RUN esy x demo1