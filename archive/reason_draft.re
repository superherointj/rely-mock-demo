/* https://reasonml.github.io/docs/en/module */

module type TestingDependencies = (Item: Comparable) => {
    type backingType;
    let empty: backingType;
    let add: (backingType, Item.t) => backingType;
  }; 
  
  module type ActualComponent = {
    /* the BaseComponent signature is copied over */
    include BaseComponent;
    let render: unit => string;
  };
  
  
  