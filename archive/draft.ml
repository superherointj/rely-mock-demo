(* YojsonBasic *)

module YojsonBasic : sig
    val from_file : string -> Yojson.Basic.t
end = struct
    let from_file filename = Yojson.Basic.from_string(
            "{\"name\": \"PRODUCTION\", \"age\": 21 }"
        )
end;;

module Yojson : sig
    include (module type of Yojson)
    module Basic : sig
        include (module type of Yojson.Basic)
        include (module type of YojsonBasic)
    end;;    
end = struct
    include Yojson
    module Basic = struct
        include Yojson.Basic
        include YojsonBasic
    end
end;;


(* let from_file s = (Dependencies.YojsonBasic.from_file s) *)



(* module type YojsonBasic = sig
    val from_file : string -> Yojson.Basic.t
end;; 

module YojsonBasic = struct
    (* Yojson.Basic.from_file *)
    let from_file filename = Yojson.Basic.from_string(
            "{\"name\": \"PRODUCTION\", \"age\": 21 }"
        )
end  *)
