

(* Dependencies *)

module type Dependencies = sig
    module Yojson : (module type of Yojson)
end;;

module ProductionDependencies : Dependencies = struct
    module Yojson = Yojson
end


(* MakeDependencies *)

module MakeDependencies (Deps : Dependencies) = struct
    include Deps
end

