
(* Logger *)

(* module type Logger = sig 
    val log : string -> unit
end;;

module Logger = struct
    let log s = Console.log ("INTJ => " ^ s)
end

module Make (L : Logger): Logger = struct
    let log s = (L.log s)
end *)

(* Yojson.Basic.from_file *)

open Yojson;;


module type YojsonBasic = sig
    val from_file : string -> Yojson.Basic.t
end;;

module YojsonBasic = struct
    let log s = Console.log ("INTJ => " ^ s)
end

module MakeYojsonBasic (L : YojsonBasic): YojsonBasic = struct
    let log s = (L.log s)
end






(* let json = Yojson.Basic.from_file batci_json in 

    val from_file : ?⁠buf:Bi_outbuf.t -> ?⁠fname:string -> ?⁠lnum:int -> string -> t
 
*)

(* let () = Lwt_main.run (Lwt_io.printf "Hello, world!\n") *)
