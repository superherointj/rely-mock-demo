

module Deps = Demo1.MakeDependencies(Demo1.ProductionDependencies)

module Yojson = Deps.Yojson

let () =
    let open Yojson.Basic.Util in
        let json = Yojson.Basic.from_file "./test/sampleData/somefile.json" in
            let name =  json |> member "name" |> to_string in
                print_endline name
