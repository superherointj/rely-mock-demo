open TestFramework;




/* Logger */

let mockLog = Mock.mock1(_ => ());

module MyLoggerMocked =
  Demo1.Make({
    let log = Mock.fn(mockLog);
  });

let {describe} =
  describeConfig
  |> withLifecycle(testLifecycle =>
       testLifecycle |> beforeEach(() => Mock.reset(mockLog))
     )
  |> build;


describe("First Mock Test - Logger", ({test}) => {

  test("Should log a start message", ({expect}) => {
    MyLoggerMocked.log("START :)");
    expect.mock(mockLog).toBeCalledWith("START :)");
  });

  test("should log a completion message", ({expect}) => {
    MyLoggerMocked.log("END :(");
    expect.mock(mockLog).toBeCalledWith("END :(");
  });

}); 

