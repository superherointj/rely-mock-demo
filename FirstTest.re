open TestFramework;

/* Mocking Yojson */

let yojsonBasicFromFileMock = Mock.mock1(filename => 
  switch (filename) {
  | "package.json" => Yojson.Basic.from_string("{\"name\": \"MOCKED\", \"age\": 27 }")
  | _ => Yojson.Basic.from_string("{\"name\": \"BAD\", \"age\": 99 }")
  }
);
let yojsonBasicFromFileMockFunction = Mock.fn(yojsonBasicFromFileMock);

module type Yojson = (module type of Yojson);

module YojsonMocked: Yojson = {
  include Yojson;
  module Basic {
    include Yojson.Basic;
    let from_file = (~buf=?, ~fname=?, ~lnum=?, filename) => {
      yojsonBasicFromFileMockFunction(filename);
    }
  }
};

module TestingDependencies : Demo1.Dependencies = {
    module Yojson = YojsonMocked;
}

module Deps = Demo1.MakeDependencies(TestingDependencies)

module Yojson = Deps.Yojson


/* Testing */

let {describe} =
  describeConfig
  |> withLifecycle(testLifecycle =>
       testLifecycle |> beforeEach(() => Mock.reset(yojsonBasicFromFileMock))
     )
  |> build;


describe("Yojson.Basic.from_file Mock Test", ({test}) => {

  test("check filename", ({expect}) => {
    let _json = Yojson.Basic.from_file("package.json");
    expect.mock(yojsonBasicFromFileMock).toBeCalledWith("package.json");
  });

  test("should return", ({expect}) => {
    let _json = Yojson.Basic.from_file("package.json");
    expect.mock(yojsonBasicFromFileMock).toReturnWith(Yojson.Basic.from_string("{\"name\": \"MOCKED\", \"age\": 27 }"));
  });

});
